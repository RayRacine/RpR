#lang scribble/manual

@title[#:tag "top"]{@bold{PGM} Exploring Probabilistic Graphical Models}

@declare-exporting[("pgm.rkt")]

@table-of-contents[]

by Ray Racine (@tt{ray dot racine at gmail dot com})

This library provides a set of code for exploring Probilistic Graphical Models.

@include-section["factor.scrbl"]

